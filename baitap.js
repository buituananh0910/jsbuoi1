// bai 1 
// input
var input = 5; 

// todo
const luong = 100000;
var output = input * luong;

// out put 
console.log('output: ', output);


// bai2
// input
var input1 = 5;
var input2 = 3;
var input3 = 2;
var input4 = 6;
var input5 = 7;

// todo
var output = (input1+input2+input3+input4+input5)/5;

// output
console.log('output: ', output);


// bai3
// input
var input = 3;

// todo
const usd = 23500;
var output = input * usd;

// output
console.log('output: ', output);


// bai4
// input
var chieudai = 5;
var chieurong = 2;

// todo
var chuvi = ( chieudai + chieurong ) * 2;
var dientich = chieudai * chieurong;

// output
console.log('chuvi: ', chuvi);
console.log('dientich: ', dientich);


// bai5
// input
var input = 53;

// todo
var donvi = input % 10;
var hangchuc = ( input - donvi ) / 10;
var output = donvi + hangchuc;

// output
console.log('output: ', output);
